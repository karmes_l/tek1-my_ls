##
## Makefile for  in /home/karmes_l/Projets/Maths
## 
## Made by lionel karmes
## Login   <karmes_l@epitech.net>
## 
## Started on  Mon Nov  3 16:51:51 2014 lionel karmes
## Last update Sun Nov 30 22:30:12 2014 lionel karmes
##

CC	= gcc

RM	= rm -f

CFLAGS	+= -Wextra -Wall -Werror
CFLAGS	+= -ansi -pedantic
CFLAGS	+= -I./include/

LDFLAGS	=

NAME	= ls

LIB	= libmy

SRCS	= main.c \
	collect_files.c \
	count_num.c \
	filtre.c \
	int_to_str.c \
	message.c \
	my_files.c \
	my_ls.c \
	my_putnbr.c \
	my_realloc.c \
	my_sort_tab2.c \
	my_strcmp2.c \
	print_result.c \
	test.c

OBJS	= $(SRCS:.c=.o)


all: $(NAME)

$(NAME): $(OBJS)
	$(CC) $(OBJS) -o $(NAME) $(LDFLAGS) -lmy -L./lib

clean:
	$(RM) $(OBJS)

fclean: clean
	$(RM) $(NAME)

re: fclean all

.PHONY: all clean fclean re
