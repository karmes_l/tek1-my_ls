/*
** print_result.c for  in /home/karmes_l/Projets/Systeme_Unix/my_ls
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Wed Nov 26 16:41:17 2014 lionel karmes
** Last update Sun Nov 30 20:16:56 2014 lionel karmes
*/

#include "my.h"

void		print_file(char *print, int o)
{
  static char	*print_file_end = NULL;
  static char	*print_dir_end = NULL;

  if (o == NO_PRINT_FILE)
    print_file_end = my_realloc(print_file_end, print);
  else if (o == NO_PRINT_DIR)
    print_dir_end = my_realloc(print_dir_end, print);
  if (o == PRINT)
    {
      if (print_file_end != NULL)
	{
	  my_putstr(print_file_end);
	  free(print_file_end);
	}
      if (print_dir_end != NULL)
	{
	  my_putstr(print_dir_end);
	  free(print_dir_end);
	}
    }
}
