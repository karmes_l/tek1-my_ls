/*
** my_ls.c for  in /home/karmes_l/Projets/Systeme_Unix/my_ls
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Tue Nov 25 19:57:44 2014 lionel karmes
** Last update Sun Nov 30 22:14:13 2014 lionel karmes
*/

#include "my.h"

void		free_ptr(t_files *ptr_files, t_options *ptr_options)
{
  int	i;

  i = 0;
  while (i < ptr_files->nbr_file)
    {
      free(ptr_files->file[i]);
      i++;
    }
  free(ptr_files->file);
  free(ptr_options->option);
}

int		my_ls(int ac, char **av)
{
  t_files	ptr_files;
  t_options	ptr_options;
  int		i;

  if (!init(ac, av, &ptr_files, &ptr_options))
    return (0);
  i = 0;
  my_sort_tab2(&ptr_files.file, ptr_files.nbr_file);
  while (i < ptr_files.nbr_file)
    {
      my_files(&ptr_files, i, &ptr_options);
      i++;
    }
  print_file("", PRINT);
  free_ptr(&ptr_files, &ptr_options);
  return (1);
}
