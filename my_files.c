/*
** my_files.c for  in /home/karmes_l/TP/my_ls
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Mon Nov 24 14:19:22 2014 lionel karmes
** Last update Sun Nov 30 21:13:27 2014 lionel karmes
*/

#include "my.h"

void		supp_directory_current(char *path)
{
  int		i;
  int		j;

  j = 0;
  while (j < 2)
    {
      i = 0;
      while (i < my_strlen(path))
	{
	  path[i] = path[i + 1];
	  i++;
	}
      j++;
    }
}

char		*parent_path(char *path)
{
  int		i;
  short		parent;
  char		*file_name;
  int		j;

  i = my_strlen(path);
  parent = 0;
  j = 0;
  if ((file_name = malloc(sizeof(char) * (my_strlen(path) - 1))) == NULL)
    exit(0);
  while (i > 0 && !parent)
    {
      if (path[i - 1] == '/')
	{
	  path[i] = '\0';
	  parent = 1;
	}
      else
	  file_name[j++] = path[i - 1];
      i--;
    }
  file_name[j] = '\0';
  my_revstr(file_name);
  return (file_name);
}

int		if_directory(t_files *ptr_files, int file, t_options *ptr_options)
{
  DIR		*dir;

  if ((dir = opendir(ptr_files->file[file])) != NULL &&
      (!my_strstr(ptr_options->option, "d")))
    {
      return (1);
    }
  if (errno == EACCES)
    {
      message2(ptr_files->file[file]);
      return (1);
    }
  return (0);
}

void		if_file(t_files *ptr_files, int file, t_options *ptr_options)
{
  DIR		*dir;
  struct dirent	*entry;
  struct stat	sb;
  char		*file_name;
  char		*path_modif;

  if ((path_modif = malloc(sizeof(char) *
			   (my_strlen(ptr_files->file[file]) - 1))) == NULL)
    exit(0);
  my_strcpy(path_modif, ptr_files->file[file]);
  if (errno == ENOTDIR || my_strstr(ptr_options->option, "d"))
    file_name = parent_path(path_modif);
  errno = 0;
  if ((dir = opendir(path_modif)) != NULL)
    {
      while ((entry = readdir(dir)) != NULL)
      	{
	  if (lstat(ptr_files->file[file], &sb) > -1 &&
	      my_strcmp(file_name, entry->d_name) == 0)
	    info_file(sb, ptr_files->file[file], ptr_options, NO_PRINT_FILE);
      	}
      closedir(dir);      
    }
  free(path_modif);
  free(file_name);
}

int		my_files(t_files *ptr_files, int file, t_options *ptr_options)
{
  if (!if_directory(ptr_files, file, ptr_options))
    {
      if (errno == ENOENT)
	message1(ptr_files->file[file]);
      else
	{
	  if_file(ptr_files, file, ptr_options);
	  if (errno == ENOTDIR)
	    message3(ptr_files->file[file]);
	}
    }
  return (0);
}
