/*
** my_realloc.c for  in /home/karmes_l/Projets/Systeme_Unix/my_ls
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Sat Nov 29 18:53:35 2014 lionel karmes
** Last update Sun Nov 30 22:15:16 2014 lionel karmes
*/

#include <stdlib.h>

char	*my_realloc_update(char *str_end, char *str, int i)
{
  int	j;

  if (str_end == NULL)
    exit(0);
  j = 0;
  while (j < my_strlen(str))
    {
      str_end[i + j] = str[j];
      j++;
    }
  str_end[i + j] = '\0';
  return (str_end);
}

char	*my_realloc(char *str_tmp, char *str)
{
  int	i;
  char	*str_end;

  i = 0;
  if (str_tmp != NULL)
    {
      if ((str_end = malloc(sizeof(char) * (my_strlen(str_tmp)
					    + my_strlen(str) + 1))) != NULL)
	{
	  while (str_tmp[i] != '\0')
	    {
	      str_end[i] = str_tmp[i];
	      i++;
	    }
	}
    free(str_tmp);
    }
  else
    str_end = malloc(sizeof(char) * (my_strlen(str) + 1));
  str_end = my_realloc_update(str_end, str, i);
 return (str_end);
}
