/*
** main.c for  in /home/karmes_l/Projets/Systeme_Unix/my_ls
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Tue Nov 25 16:42:13 2014 lionel karmes
** Last update Sun Nov 30 22:17:34 2014 lionel karmes
*/

#include "my.h"

int	main(int ac, char **av)
{
  if (!my_ls(ac, av))
    return (1);
  return (0);
}
