/*
** print_result.c for  in /home/karmes_l/Projets/Systeme_Unix/my_ls
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Wed Nov 26 16:41:17 2014 lionel karmes
** Last update Sun Nov 30 22:28:47 2014 lionel karmes
*/

#include "my.h"

int		sizeof_print(struct stat sb, char *path, t_options *ptr_options)
{
  int		size_print;

  size_print = my_strlen(path);
  if (my_strstr(ptr_options->option, "l"))
    {
      size_print += 10;
      size_print += my_strlen(int_to_str((long) sb.st_nlink));
      size_print += my_strlen((getpwuid(sb.st_uid))->pw_name);
      size_print += my_strlen((getgrgid(sb.st_gid))->gr_name);
      size_print += my_strlen(int_to_str((long) sb.st_size));
      size_print += 13;
    }
  size_print += 1;
  return (size_print);
}

char		*droits_in_str(struct stat sb)
{
  char		*droits;

  if ((droits = malloc(sizeof(char) * 10 + 1)) == NULL)
    exit(0);
  droits[10] = '\0';
  (MODE(S_IFLNK) ? (droits[0] = 'l') : (droits[0] = '-'));
  (MODE(S_IFDIR) ? (droits[0] = 'd') : (droits[0] = droits[0]));
  (MODE(S_IRUSR) ? (droits[1] = 'r') : (droits[1] = '-'));
  (MODE(S_IWUSR) ? (droits[2] = 'w') : (droits[2] = '-'));
  (MODE(S_IXUSR) ? (droits[3] = 'x') : (droits[3] = '-'));
  (MODE(S_IRGRP) ? (droits[4] = 'r') : (droits[4] = '-'));
  (MODE(S_IWGRP) ? (droits[5] = 'w') : (droits[5] = '-'));
  (MODE(S_IXGRP) ? (droits[6] = 'x') : (droits[6] = '-'));
  (MODE(S_IROTH) ? (droits[7] = 'r') : (droits[7] = '-'));
  (MODE(S_IWOTH) ? (droits[8] = 'w') : (droits[8] = '-'));
  (MODE(S_IXOTH) ? (droits[9] = 'x') : (droits[9] = '-'));
  return (droits);
}

int		format_l_time(struct stat sb, char *print, int offset)
{
  char		*time;
  char		*time_complet;
  int		i;
  int		j;

  if ((time_complet = malloc(sizeof(char) * 25)) == NULL)
    exit(0);
  time_complet[25] = '\0';
  my_strcpy(time_complet, ctime(&sb.st_mtime));
  if ((time = malloc(sizeof(char) * 13)) == NULL)
    exit(0);
  time[12] = '\0';
  j = 0;
  i = 0;
  while (i < 16)
    {
      if (i > 3)
	  time[j++] = time_complet[i];
      i++;
    }
  my_strncpy2(print, time, offset - 12, offset += 12);
  free(time);
  free(time_complet);
  return (offset);
}

void		format_l(struct stat sb, char *print, char *path)
{
  int		offset;
  char		*droits;

  offset = 0;
  droits = droits_in_str(sb);
  my_strncpy2(print, droits, offset - 10, offset += 10);
  free(droits);
  my_strncpy2(print, int_to_str((long) sb.st_nlink),
	      offset - my_strlen(int_to_str((long) sb.st_nlink)),
	      offset += my_strlen(int_to_str((long) sb.st_nlink)));
  my_strncpy2(print, (getpwuid(sb.st_uid))->pw_name,
	      offset - my_strlen((getpwuid(sb.st_uid))->pw_name),
	      offset += my_strlen((getpwuid(sb.st_uid))->pw_name));
  my_strncpy2(print, (getgrgid(sb.st_gid))->gr_name,
	      offset - my_strlen((getgrgid(sb.st_gid))->gr_name),
	      offset += my_strlen((getgrgid(sb.st_gid))->gr_name));
  my_strncpy2(print, int_to_str((long) sb.st_size),
	      offset - my_strlen(int_to_str((long) sb.st_size)),
  	      offset += my_strlen(int_to_str((long) sb.st_size)));
  offset = format_l_time(sb, print, offset);
  my_strncpy2(print, path, offset - my_strlen(path), offset += my_strlen(path));
  my_strncpy2(print, "\n", offset, 1);
}

void		info_file(struct stat sb, char *path, t_options *ptr_options, int o)
{
  char		*print;
  int		size_print;

  if (o == NO_PRINT_FILE)
    supp_directory_current(path);
  size_print = sizeof_print(sb, path, ptr_options);
  if ((print = malloc(sizeof(char) * (size_print + 1))) == NULL)
    exit(0);
  if (my_strstr(ptr_options->option, "l"))
    format_l(sb, print, path);
  else
    {
      my_strcpy(print, path);
      my_strcat(print, " ");
    }
  print_file(print, o);
  free(print);
}
