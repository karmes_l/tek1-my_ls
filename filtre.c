/*
** filtre.c for  in /home/karmes_l/Projets/Systeme_Unix/my_ls
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Tue Nov 25 16:43:39 2014 lionel karmes
** Last update Sun Nov 30 21:12:37 2014 lionel karmes
*/

#include "my.h"

void	list_files(char *str, t_files *ptr_files)
{
  int	i;

  i = 0;
  if (str[i] != '-' && my_strlen(str) > 0)
    {
      if ((ptr_files->file[ptr_files->nbr_file] = malloc(sizeof(char) *
							 (my_strlen(str) + 3))) == NULL)
							exit(0);
      my_strcpy(ptr_files->file[ptr_files->nbr_file], "./");
      my_strncpy2(ptr_files->file[ptr_files->nbr_file], str,
		  2, my_strlen(str));
      ptr_files->file[ptr_files->nbr_file][my_strlen(str) + 2] = '\0';
      ptr_files->nbr_file++;
    }
}

int	filtre(char *str)
{
  int	i;
  char	option_char[5];
  int	j;

  i = 0;
  my_strcpy(option_char, "lRdrt");
  if (str[i] == '-')
    {
      while (++i < my_strlen(str))
	{
	  j = 0;
	  while (str[i] != option_char[j] && j < 5)
	    j++;
	  if (j == 5)
	    {
	      my_putstr("ls: invalid option -- '");
	      my_putchar(str[i]);
	      my_putstr("'\nTry 'ls --help' for more information.\n");
	      return (0);
	    }
	}
      return (1);
    }
  return (1);
}

void	init_ptr(t_files *ptr_files, int ac, t_options *ptr_options,
		       char **av)
{
  int	len_option;
  int	i;

  i = 0;
  len_option = 0;
  while (i < ac - 1)
    {
      if (av[i][0] == '-')
	len_option += my_strlen(av[i]);
	  i++;
    }
  ptr_files->nbr_file = 0;
  if ((ptr_files->file = malloc(sizeof(char *) * ac)) == NULL)
    exit(0);
  if ((ptr_options->option = malloc(sizeof(char) * (len_option + 1))) == NULL)
    exit(0);
  ptr_options->option[0] = '\0';
}

int	init(int ac, char **av, t_files *ptr_files, t_options *ptr_options)
{
  int	k;

  k = 1;
  init_ptr(ptr_files, ac, ptr_options, av);
  while (k < ac)
    {
      if (!(filtre(av[k])))
	return (0);
      else if (av[k][0] == '-')
	my_strcat(ptr_options->option, av[k]);
      list_files(av[k], ptr_files);
      k++;
    }
  if (ptr_files->nbr_file == 0)
    {
      if ((ptr_files->file[0] = malloc(sizeof(char) * 3)) == NULL)
	exit(0);
      my_strcpy(ptr_files->file[0], "./");
      ptr_files->nbr_file++;
    }
  ptr_files->file[ptr_files->nbr_file] = '\0';
  return (1);
}
