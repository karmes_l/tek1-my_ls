/*
** my.h for  in /home/karmes_l/test/tmp_Piscine_C_J09
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Thu Oct  9 15:22:30 2014 lionel karmes
** Last update Sun Nov 30 22:24:44 2014 lionel karmes
*/

#ifndef MY_H_
# define MY_H_

# include "files.h"
# include <sys/types.h>
# include <dirent.h>
# include <stdlib.h>
# include <unistd.h>
# include <sys/stat.h>
# include <errno.h>
# include <pwd.h>
# include <grp.h>
# include <time.h>

void	my_putchar(char);
int	my_isneg(int);
void	my_putnbr(long);
int	my_swap(int *, int *);
int	my_putstr(char *);
int	my_strlen(char *);
int	my_getnbr(char *);
void	my_sort_int_tab(int *, int);
int	my_power_ec(int, int);
int	my_square_root(int);
int	my_is_prime(int);
int	my_find_prime_sup(int);
char	*mu_strcpy(char *, char *);
char	*my_strncpy(char *, char *, int);
char	*my_revstr(char *);
char	*my_strstr(char *, char *);
int	my_strcmp(char *, char *);
int	my_strcmpZ(char *, char *);
int	my_strncmp(char *, char *, int n);
int	my_strcpy(char *, char *);
char	*my_strupcase(char *);
char	*my_strlowcase(char *);
char	*mystrcapitalize(char *);
int	my_str_isalpha(char *);
int	my_str_isnum(char *);
int	mu_str_islower(char *);
int	my_str_isupper(char *);
int	my_str_isprintable(char *);
int	my_showstr(char *);
int	my_showmen(char *, int);
char	*my_strcat(char *, char *);
char	*my_strncat(char *, char *, int);
int	my_strlcat(char *, char *, int);
void	f_101pong(char **);
int	arg_valid(int, char **);
int	my_str_isnum2(char *);
char	*my_strncpy2(char *, char *, int , int);
void	supp_directory_current(char *);
char	*parent_path(char *);
int	if_directory(t_files *, int, t_options *);
  void	if_file(t_files *, int, t_options *);
int	my_files(t_files *, int,  t_options *);
int	init(int, char **, t_files *, t_options *);
void	my_sort_tab2(char ***, int);
void	info_file(struct stat, char *, t_options *, int);
void	info_directory(DIR *, t_options *, t_files *, int);
void	print_file(char *, int);
char	*my_realloc(char *, char *);
int	count_num(long);
char	*int_to_str(long);
int	sizeof_print(struct stat, char *, t_options *);
char	*droits_in_str(struct stat);
int	format_l_time(struct stat, char *, int);
void	format_l(struct stat, char *, char *);
void	list_files(char *, t_files *);
int	filtre(char *);
void	init_ptr(t_files *, int, t_options *, char **);
void	message1(char *);
void	message2(char *);
void	message3(char *);
void	free_ptr(t_files *, t_options *);
int	my_ls(int, char **);
char	*my_realloc_update(char *, char *, int);

# define NO_PRINT_DIR (2)
# define NO_PRINT_FILE (1)
# define PRINT (0)
# define MODE(v1) (((sb.st_mode) & (v1)) == (v1))

#endif /* !MY_H_ */
