/*
** files.h for  in /home/karmes_l/Projets/Systeme_Unix/my_ls/include
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Tue Nov 25 18:02:05 2014 lionel karmes
** Last update Wed Nov 26 16:58:34 2014 lionel karmes
*/

#ifndef FILES_H_
# define FILES_H_

typedef struct	s_files
{
  int		nbr_file;
  char		**file;
}		t_files;

typedef struct	s_options
{
  char		*option;
}		t_options;

#endif /* !FILES_H_ */
