/*
** my_sort_tab2.c for  in /home/karmes_l/Projets/Systeme_Unix/my_ls
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Wed Nov 26 17:41:54 2014 lionel karmes
** Last update Sat Nov 29 18:44:42 2014 lionel karmes
*/

#include "my.h"

void	my_sort_tab2(char ***tab, int j)
{
  char	*tmp;
  int	i;
  short	modif_tab;

  modif_tab = 1;
  while (modif_tab)
    {
      i = 1;
      modif_tab = 0;
      while (i < j)
	{
	  if (my_strcmp2(*(*tab + i - 1), *(*tab + i)) > 0)
	    {
	      tmp = *(*tab + i - 1);
	      *(*tab + i - 1) = *(*tab + i);
	      *(*tab + i) = tmp;
	      modif_tab = 1;
	    }
	  i++;
	}
    }
}
