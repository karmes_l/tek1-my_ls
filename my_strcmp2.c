/*
** my_strcmp.c for  in /home/karmes_l/test/tmp_Piscine_C_J06
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Mon Oct  6 19:34:18 2014 lionel karmes
** Last update Fri Nov 28 21:21:32 2014 lionel karmes
*/

int	my_strcmp2(char *s1, char *s2)
{
  int	i;
  int	a;
  int	b;

  i = 0;
  while (s1[i] != '\0')
    {
      ((s1[i] >= 'A' && s1[i] <= 'Z') ? (a = 32) : (a = 0));
      ((s2[i] >= 'A' && s2[i] <= 'Z') ? (b = 32) : (b = 0));
      if ((s1[i] + a) - (s2[i] + b) != 0)
	return ((s1[i] + a) - (s2[i] + b));
      i = i + 1;
    }
  if (s2[i] != '\0')
    return (-s2[i]);
  else
    return (0);
}
