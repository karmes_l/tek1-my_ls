/*
** my_isneg.c for my_isneg in /home/karmes_l/test/tmp_Piscine_C_J03
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Wed Oct  1 14:23:47 2014 lionel karmes
** Last update Wed Oct  8 10:42:17 2014 lionel karmes
*/

int	my_isneg(int n)
{
  if (n >= 0)
    {
      my_putchar('P');
    }
  else
    {
      my_putchar('N');
    }
}
