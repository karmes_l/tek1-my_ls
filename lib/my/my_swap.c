/*
** my_swap.c for my_swap in /home/karmes_l/test/tmp_Piscine_C_J04
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Thu Oct  2 10:54:40 2014 lionel karmes
** Last update Thu Oct 23 16:53:08 2014 lionel karmes
*/

int	my_swap(int *a, int *b)
{
  int	tmp_b;

  tmp_b = *b;
  *b = *a;
  *a = tmp_b;
}
