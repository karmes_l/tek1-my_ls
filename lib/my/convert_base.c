/*
** convert_base.c for  in /home/karmes_l/test/tmp_Piscine_C_J08
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Wed Oct  8 17:16:10 2014 lionel karmes
** Last update Wed Oct 29 14:08:11 2014 lionel karmes
*/

#include <stdlib.h>

int	size_table(int nbr, int	nbr_base)
{
  int	i;

  i = 1;
  while (nbr / nbr_base > 0)
    {
      nbr = nbr / nbr_base;
      i = i + 1;
    }
  return (i);
}

int	value_from_base(char nbr, char *base_from)
{
  int	i;

  i = 0;
  while (base_from[i] != '\0')
    {
      if (*(base_from + i) == nbr)
	return (i);
      i = i + 1;
    }
  return (0);
}

int	convert_base_from(char *nbr, char *base_from)
{
  int	nbr_base;
  int	nbr_base_from;
  int	nbr_b10;
  int	i;
  int	value_base;

  i = 0;
  nbr_b10 = 0;
  nbr_base = my_strlen(nbr);
  nbr_base_from = my_strlen(base_from);
  while (nbr_base > 0)
    {
      nbr_base = nbr_base - 1;
      value_base = value_from_base(nbr[i], base_from);
      nbr_b10 = nbr_b10 + value_base * power(nbr_base_from, nbr_base);
      i = i + 1;
    }
  return (nbr_b10);
}

char	*convert_base_to(int nbr, char *base)
{
  int	nbr_base;
  int	reste;
  int	i;
  char	*str;

  nbr_base = my_strlen(base);
  i = size_table(nbr, nbr_base);
  str = malloc(i);
  i = 0;
  while (nbr / nbr_base > 0)
    {
      reste = nbr % nbr_base;
      nbr = nbr / nbr_base;
      *(str + i) = base[reste];
      i = i + 1;
    }
  reste = nbr % nbr_base;
  *(str + i) = base[reste];
  return (str);
}

char	*convert_base(char *nbr, char *base_from, char *base_to)
{
  int	nb_b10;
  char	*str_positive;
  char	*str_negative;
  int	i;

  if (my_strlen(base_from) <= 1 || my_strlen(base_to) <= 1)
    return (0);
  nb_b10 = convert_base_from(nbr, base_from);
  str_positive = convert_base_to(nb_b10, base_to);
  my_revstr(str_positive);
  if (nbr[0] == '-')
    {
      str_negative = malloc(my_strlen(str_positive) + 1);
      *(str_negative + 0) = '-';
      i = 1;
      while (str_negative[i] != '\0')
	{
	  *(str_negative + i) = *(str_positive + i - 1);
	}
      return (str_negative);
    }
  return (str_positive);
}
