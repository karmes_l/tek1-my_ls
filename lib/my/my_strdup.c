/*
** my_strdup.c for  in /home/karmes_l/test/tmp_Piscine_C_J08
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Wed Oct  8 16:46:51 2014 lionel karmes
** Last update Wed Oct  8 17:11:02 2014 lionel karmes
*/

#include <stdlib.h>

char	*my_strdup(char *src)
{
  int	length;
  char	*str;

  length = my_strlen(src);
  str = malloc(length);
  my_strcpy(str, src);
  return str;
}
