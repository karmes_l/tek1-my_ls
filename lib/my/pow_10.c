/*
** pow_10.c for  in /home/karmes_l/test/my_function
** 
OP** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Thu Oct  9 08:50:35 2014 lionel karmes
** Last update Sat Oct 11 08:51:36 2014 lionel karmes
*/

int	pow_10(int i)
{
  int	c;
  int	nb;

  c = 0;
  nb = 1;
  while (c < i)
    {
      nb = nb * 10;
      c = c + 1;
    }
  return (nb);
}
