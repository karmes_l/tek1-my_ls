/*
** my_str_to_wordtab.c for  in /home/karmes_l/test/tmp_Piscine_C_J08
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Thu Oct  9 12:31:23 2014 lionel karmes
** Last update Thu Oct 23 16:52:46 2014 lionel karmes
*/

#include <stdlib.h>

int	nbr_words(char *str)
{
  int	i;
  int	j;
  int	isnalpha;

  i = 0;
  j = 0;
  if (my_charisalpha(str[0]) == 0)
    j = j + 1;
  while (str[i] != 0)
    {
      if (my_charisalpha(str[i]) != 0)
	isnalpha = 1;
      if (str[i + 1] != 0)
	{
	  if (isnalpha == 1 && my_charisalpha(str[i + 1]) == 0)
	    {
	      isnalpha = 0;
	      j = j + 1;
	    }
	}
      i = i + 1;
    }
  return (j);
}

int	size_word(int i, char *str)
{
  int	j;

  j = 0;
  while (str[i] != '\0' && my_charisalpha(str[i]) == 0)
    {
      j = j + 1;
      i = i + 1;
    }
  return (j);
}

char	**size_table_char(char *str)
{
  int	i;
  int	j;
  int	k;
  char	**mot;

  k = 0;
  mot = malloc((1 + nbr_words(str)) * sizeof(char *));
  i = 0;
  while (str[i] != '\0')
    {
      j = size_word(i, str);
      if (j > 0)
	{
	  i = i + j;
	  mot[k] = malloc(j * sizeof(char) + 1);
	  k = k + 1;
	}
      else
	  i = i + j + 1;
    }
  mot[k] = malloc(sizeof(char));
  mot[k] = 0;
  return (mot);
}

char **my_str_to_wordtab(char *str)
{
  char	**mot;
  int	i;
  int	j;
  int	k;

  i = 0;
  k = 0;
  mot = size_table_char(str);
  while (str[i] != '\0')
    {
      j = 0;
      while (my_charisalpha(str[i + j]) == 0)
	{
	  mot[k][j] = *(str + i + j);
	  j = j + 1;
	}
      if (j > 0)
	{
	  i = i + j;
	  k = k + 1;
	}
      else
	  i = i + 1;
    }
  return (mot);
}
