/*
** my_strncpy.c for  in /home/karmes_l/test/tmp_Piscine_C_J06
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Mon Oct  6 16:11:48 2014 lionel karmes
** Last update Sun Nov 30 11:29:43 2014 lionel karmes
*/

char	*my_strncpy2(char *dest, char *src, int k, int n)
{
  int	i;
  int	j;

  i = 0;
  j = 0;
  while (i < k)
    i++;
  while (i < k + n && src[j] != '\0')
    {
      dest[i] = src[j];
      i = i + 1;
      j++;
    }
  if (src[j] == '\0')
    {
      dest[i + 1] = '\0';
    }
  return dest;
}
