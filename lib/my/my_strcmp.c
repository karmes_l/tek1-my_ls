/*
** my_strcmp.c for  in /home/karmes_l/test/tmp_Piscine_C_J06
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Mon Oct  6 19:34:18 2014 lionel karmes
** Last update Wed Nov 26 11:41:32 2014 lionel karmes
*/

int	my_strcmp(char *s1, char *s2)
{
  int	i;

  i = 0;
  while (s1[i] != '\0')
    {
      if (s1[i] - s2[i] != 0)
	return (s1[i] - s2[i]);
      i = i + 1;
    }
  if (s2[i] != '\0')
    return (-s2[i]);
  else
    return (0);
}
