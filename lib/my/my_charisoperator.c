/*
** my_charisoperator.c for  in /home/karmes_l/test/tmp_Piscine_C_J11/do-op
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Mon Oct 20 16:49:32 2014 lionel karmes
** Last update Mon Oct 20 18:58:54 2014 lionel karmes
*/

int	my_charisoperator(char c)
{
  if (c == '+' || c == '-' || c == '/' || c == '%' || c == '*')
    return (0);
  return (1);
}
