/*
** my_charisprintable.c for  in /home/karmes_l/test/my_function
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Thu Oct 23 17:49:04 2014 lionel karmes
** Last update Thu Oct 23 17:59:05 2014 lionel karmes
*/

int	my_charisprintable(char c)
{
  if (c < 32 || c > 126)
    return (1);
  return (0);
}
