/*
** message.c for  in /home/karmes_l/Projets/Systeme_Unix/my_ls
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Wed Nov 26 10:27:08 2014 lionel karmes
** Last update Sun Nov 30 20:12:14 2014 lionel karmes
*/

#include "my.h"

void	message1(char *path)
{
  char	*str;

  str = malloc(sizeof(char) * (my_strlen(path) + 1));
  my_putstr("ls: cannot access ");
  my_strcpy(str, path);
  supp_directory_current(str);
  my_putstr(str);
  my_putstr(": No such file or directory\n");
  free(str);
}

void	message2(char *path)
{
  char	*str;

  str = malloc(sizeof(char) * (my_strlen(path) + 1));
  my_putstr("ls: cannot open directory ");
  my_strcpy(str, path);
  supp_directory_current(str);
  my_putstr(str);
  my_putstr(": Permission denied\n");
  free(str);
}

void	message3(char *path)
{
  char	*str;

  str = malloc(sizeof(char) * (my_strlen(path) + 1));
  my_putstr("ls: cannot access ");
  my_strcpy(str, path);
  supp_directory_current(str);
  my_putstr(str);
  my_putstr(": Not a directory\n");
  free(str);
}
